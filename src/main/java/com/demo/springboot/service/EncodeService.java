package com.demo.springboot.service;

import com.demo.springboot.domain.dto.EncodeDto;
import com.demo.springboot.domain.dto.ReturnData;

import java.io.FileNotFoundException;

public interface EncodeService {
    ReturnData getEncodeString(EncodeDto wynik) throws FileNotFoundException;
}

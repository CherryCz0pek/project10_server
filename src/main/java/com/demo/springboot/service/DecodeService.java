package com.demo.springboot.service;

import com.demo.springboot.domain.dto.DecodeDto;
import com.demo.springboot.domain.dto.EncodeDto;
import com.demo.springboot.domain.dto.ReturnData;

import java.io.FileNotFoundException;

public interface DecodeService {
    ReturnData getDecodeString(DecodeDto wynik) throws FileNotFoundException;
}

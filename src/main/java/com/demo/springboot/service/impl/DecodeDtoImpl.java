package com.demo.springboot.service.impl;

import com.demo.springboot.domain.dto.DecodeDto;
import com.demo.springboot.domain.dto.EncodeDto;
import com.demo.springboot.domain.dto.ReturnData;
import com.demo.springboot.service.DecodeService;
import com.demo.springboot.service.EncodeService;
import org.springframework.stereotype.Service;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.io.PrintWriter;



@Service
public class DecodeDtoImpl implements DecodeService {
    @Override
    public ReturnData getDecodeString(DecodeDto wynik) throws FileNotFoundException {

        String rezultat = wynik.getDecodeMessage();
        DecodeDtoImpl obiekcik = new DecodeDtoImpl();

        String znak = wynik.getDecodeMessage();
        String nowRezultat = obiekcik.decodeMethod(znak).replace("\u0000", "");



        return new ReturnData(nowRezultat);
    }

    public String decodeMethod(String messageString) throws FileNotFoundException {

        File file = new File("key.txt");
        Scanner in = new Scanner(file);
        int key = in.nextInt();

        char message[] = messageString.toCharArray();
        int messageLength = message.length;
        int restOfWidth = (messageLength - 1) % (key - 1);
        // reszta z dzielenia: (n - 1) % (secretCode - 1) -- pokazuje nam przez jaką ilość wierszy kolumna jest szersza.

        int universalTableWidth;
        double tableWidthDouble = (messageLength - 1) / (key - 1);
        if ((messageLength-1) % (key - 1)!=0) {
            universalTableWidth = (int) tableWidthDouble + 1;
        }
        else{
            universalTableWidth = (messageLength - 1) / (key - 1);
        }

        int i = 0;
        int j = 0;
        int k = 0;

        char universalTable[][] = new char[key][universalTableWidth];
        universalTable[i][j] = message[k];
        k++;
        j++;


        char universalTable2[][] = new char[key][universalTableWidth];
        //Sprawdzamy czy kurs jest od góry, czy od dołu, wykorzystując algorytm z klasy Encode - pewnie można inaczej, ale czas goni...
        int a = 0;
        int b = -1;
        int c = 0;
        boolean course = true;
        for (; a < messageLength; a++) {
            b = b + 1;
            universalTable2[b][c] = message[a];
            course = true;

            if (b==key-1&&a<messageLength-1) {
                c++;
                b--;
                a++;
                for (; b >= 0 & a < messageLength; a++, b--) {
                    universalTable2[b][c] = message[a];
                }
                a--;
                b++;
                c++;
                course = false;
            }
        }


        a = 0;
        if ((restOfWidth != 0 && course == false)||course == true) {
            a = 1;
        }



        for (; i < 1 & j < universalTableWidth - a; j = j + 2, k++) {
            universalTable[i][j] = message[k];
        }
        i++;


        int a2 = 0;
        for (; i<key-1; i++){
            if (course==true&&i<=restOfWidth){
                a2=0;
            }
            else if (course==true&&i>restOfWidth&&restOfWidth!=0){
                a2=1;
            }
            else if (course==true&&restOfWidth==0){
                a2=0;
            }
            else if (course == false){
                if (i >= key-1-restOfWidth){
                    a2=0;
                }
                else{
                    a2=1;
                }
            }

            else if (course==false&&i<restOfWidth){
                a2=1;
            }
            else if (course==false&&i>=restOfWidth){
                a2=0;
            }

            for (int j2 = 0; j2 < universalTable[i].length - a2; j2++, k++) {
                universalTable[i][j2] = message[k];
            }
        }
        for (int j2=0; restOfWidth==0&&course==true ? j2<universalTableWidth:j2<universalTableWidth-1; j2=j2+2, k++) {
            universalTable[i][j2] = message[k];
        }


// beep beep beep beep beep beep beep beep beep beep beep beep beep beep beep beep beep beep beep beep beep beep beep beep beep beep beep beep beep beep beep beep beep beep


        char resultTable[] = new char[message.length];


        a = 0;
        b = -1;
        c = 0;

        for (; a < messageLength; a++) {
            b = b + 1;
            resultTable[a] = universalTable[b][c];
            course = true;

            if (b==key-1&&a<messageLength-1) {
                c = c + 1;
                b = b - 1;
                a = a + 1;
                for (; b >= 0 & a < messageLength; a++, b--) {
                    resultTable[a] = universalTable[b][c];

                }
                a--;
                b++;
                c++;
                course = false;
            }

        }


        String resultString = new String(resultTable, 0, resultTable.length);
        return resultString;
    }








}








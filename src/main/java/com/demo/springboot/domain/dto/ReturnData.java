package com.demo.springboot.domain.dto;

import java.io.Serializable;


public class ReturnData implements Serializable {

    private String result;


    public ReturnData() {
    }

    public ReturnData(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    @Override
    public String toString() {
        return "SzyfrData{" +
                "result='" + result  +
                '}';
    }
}
package com.demo.springboot.domain.dto;

import java.io.Serializable;

public class DecodeDto implements Serializable {
    private String decodeMessage;

    public String getDecodeMessage() {
        return decodeMessage;
    }

    public DecodeDto() {
    }

    public DecodeDto(String decodeMessage) {
        this.decodeMessage = decodeMessage;
    }
}

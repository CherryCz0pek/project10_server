package com.demo.springboot.domain.dto;

        import java.io.Serializable;

public class EncodeDto implements Serializable {
    private String encodeMessage;

    public String getEncodeMessage() {
        return encodeMessage;
    }

    public EncodeDto() {
    }

    public EncodeDto(String encodeMessage) {
        this.encodeMessage = encodeMessage;
    }
}

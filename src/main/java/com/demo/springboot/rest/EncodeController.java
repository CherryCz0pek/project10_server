package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.EncodeDto;
import com.demo.springboot.domain.dto.ReturnData;
import com.demo.springboot.service.impl.EncodeDtoImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.FileNotFoundException;

@Controller
@RequestMapping("/api")
public class EncodeController {
   // private static final Logger LOGGER = LoggerFactory.getLogger(EncodeController.class);

    @Autowired
    private EncodeDtoImpl wynik;

    @RequestMapping(value = "/fence/encode", method = RequestMethod.POST)
    public ResponseEntity<ReturnData> testDocument(@RequestBody EncodeDto encodeDto) throws FileNotFoundException{

    //    LOGGER.info("Wiadomosc do zaszyfrowania to: {}", encodeDto.getEncodeMessage());

        final ReturnData returnData = wynik.getEncodeString(encodeDto);

        return new ResponseEntity<>(returnData, HttpStatus.OK);




    }
}
